def luhnAlgorithm(card):
    digits = len(card)
    sum = 0
    parity = (digits-2) % 2
    for i in range(digits):
        digit = int(card[i])
        if i % 2 == parity:
            digit *= 2
        if digit > 9:
            digit -= 9
        sum += digit
    return (sum % 10) == 0
    
def checkPrefix(card):
    if card.startswith('4'):
        return 'Visa'
    elif card[0] = '5' and card[1] in ['1', '2', '3', '4', '5']:
        return 'MasterCard'
    elif card.startswith('34') or card.startswith('37'):
        return 'American Express'
    elif card.startswith('6011'):
        return 'Discover'
    elif card.startswith('3'):
        return 'JCB'
    else:
        return 'Unknown'
    
if __name__ == '__main__':
    card = input('Enter your credit card number: ')
    if luhn_algorithm(card):
        print('Valid')
    else:
        print('Invalid')
    print('Prefix:', checkPrefix(card))
